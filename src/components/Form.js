import { Formik, Form, Field, ErrorMessage, FieldArray } from "formik";
import React from "react";
import * as Yup from "yup";

const onSubmit = (values) => {
  console.log("Form data", values);
};
const furmik = () => {
  const initialValues = {
    name: "",
    address: "",
    email: "",
    phoneNumbers: [],
    educationalDetails: {
      passedYear: "",
    },
  };

  const validationSchema = Yup.object({
    name: Yup.string().required("this field is required"),
    address: Yup.string().required("required"),
    email: Yup.string().email("invalid Format").required("Required!"),
  });

  return (
    <Formik
      initialValues={initialValues}
      validationSchema={validationSchema}
      onSubmit={onSubmit}
    >
      <Form>
        <div>
          <label>Name:</label>
          <Field type="text" id="name" name="name" />
          <ErrorMessage name="name" />
        </div>
        <br />
        <br />
        <div>
          <label>Address:</label>
          <Field type="text" id="address" name="address" />
          <ErrorMessage name="address" />
        </div>
        <br />
        <br />
        <div>
          <label>Email:</label>
          <Field type="email" id="email" name="email" />
          <ErrorMessage name="email" />
        </div>
        <br />
        <br />
        <div>
          <label>Primary Phone Number</label>
          <Field type="contact" id="primaryph" name="phoneNumbers[0]" />
          <ErrorMessage name="contact" />
        </div>
        <br />
        <br />
        <div>
          <label>Secondary Phone Number</label>
          <Field type="contact" id="secondaryph" name="phoneNumbers[1]" />
          <ErrorMessage name="contact" />
        </div>
        <br />
        <br />
        <div>
          <label>Passed Year</label>
          <Field
            type="passedYear"
            id="passedYear"
            name="educationalDetails.passedYear"
          />
        </div>

        <button type="submit">Submit</button>
      </Form>
    </Formik>
  );
};
export default Form;
