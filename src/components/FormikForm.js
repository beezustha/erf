import { Formik, Form, Field, ErrorMessage } from "formik";
import React, {useState} from "react";
import * as Yup from "yup";
import InputQualification from './InputQualification';
const onSubmit = (values) => {
  console.log("Form data", values);
};
const FormikForm = () => {
  const initialValues = {
    name: "",
    address: "",
    email: "",
    phoneNumbers: ["", ""],
    inputList: [{ 
                boardName: "",
                passedYear: "", 
                passedLevel: "" }],
  };
  const validationSchema = Yup.object({
    name: Yup.string().required("this field is required"),
    address: Yup.string().required("required"),
    email: Yup.string().email("invalid Format").required("Required!"),
  });
    const [inputList, setInputList] = useState([]);

    const handleChange = (e, index) => {
      const { name, values } = e.target;

      const list = [...inputList];
      list[index][name] = values;
      setInputList(list);
    };

    const handleAddInput = () => {
      const list = [...inputList];
      list.push({ boardName: "", passedYear: "", passedLevel: "" });
      setInputList(list);
      // OR setInputList([...inputList,{boardName:"", passedYear:"", passedLevel:""}]);
    };

    const handleRemoveInput = (index) => {
      const list = [...inputList];
      list.splice(index, 1);
      setInputList(list);
    };

    const handleSubmit =async (e) => {
    e.preventDefault();
    };
  return (
    <Formik
      initialValues={initialValues}
      validationSchema={validationSchema}
      onSubmit={onSubmit}
    >
      <Form onSubmit={handleSubmit} >
        <div>
          <label>Name:</label>
          <Field type="text" id="name" name="name" />
          <ErrorMessage name="name" />
        </div>
        <br />
        <br />
        <div>
          <label>Address:</label>
          <Field type="text" id="address" name="address" />
          <ErrorMessage name="address" />
        </div>
        <br />
        <br />
        <div>
          <label>Email:</label>
          <Field type="email" id="email" name="email" />
          <ErrorMessage name="email" />
        </div>
        <br />
        <br />
        <div>
          <label>Number</label>
          <Field type="contact" id="contact" name="conatact" />
          <ErrorMessage name="contact" />
        </div>
        <br />
        <br />
        <InputQualification
          qualifications={inputList}
          add={handleAddInput}
          delete={handleRemoveInput}
          OnChange={handleChange}
        />
        <button type="submit">Submit</button>
      </Form>
    </Formik>
  );
};


export default FormikForm;
