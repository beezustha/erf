import React from "react";

const InputQualification = (props) => {
  return (
    <div className="App">
      {props.inputList.map((val, index) => {
        let boardName = `boardName-${index}`,
          passedYear = `passedYear-${index}`,
          passedLevel = `passedLevel-${index}`;
        return (
          <div key={index} className="box">
            <label>Board Name</label>
            <input
              name="boardName"
              id={boardName}
              data-id={index}
              type="text"
              placeholder="Board Name"
              values={boardName}
              onChange={props.handleChange}
            />{" "}
            <label>Passed Year</label>
            <input
              name="passedYear"
              id={passedYear}
              data-id={index}
              type="text"
              placeholder="Passed Year"
              values={passedYear}
              onChange={(e) => props.handleChange(e, index)}
            />{" "}
            <label>Passed Level</label>
            <input
              name="passedLevel"
              id={passedLevel}
              data-id={index}
              type="text"
              placeholder="Passed Level"
              values={passedLevel}
              onChange={(e) => props.handleChange(e, index)}
            />{" "}
            {props.inputList.length - 1 === index && (
              <input type="button" value="Add" onClick={() => props.add()} />
            )}{" "}
            {props.inputList.length !== 1 && (
              <input
                type="button"
                value="Remove"
                onClick={() => props.delete(val)}
              />
            )}
          </div>
        );
      })}
    </div>
  );
};

export default InputQualification;
